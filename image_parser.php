<?php

require_once ("lib/simplehtmldom_1_5/simple_html_dom.php");

for($i=1;$i<4;$i++) {

    $url = "https://avatarko.ru/kartinki/dengi/$i";
    $html = file_get_html($url, $use_include_path = false, $context = null, $offset = 0, $maxLen = -1,
        $lowercase = true, $forceTagsClosed = true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN = true,
        $defaultBRText = DEFAULT_BR_TEXT, $defaultSpanText = DEFAULT_SPAN_TEXT);
    $property = 'data-id';

    foreach ($html->find('.img-thumbnail') as $img) {
        $image = $img->find('img', 0);
        $src = str_replace("avatar", "kartinka", $image->src);
        $url = 'https://avatarko.ru/' . $src;
        $save = 'images/male/den' . $img->$property . ".jpg";
        $data = file_get_contents($url);
        if ($data && strpos($url,'.gif') === false) {
            echo $save."\r\n";
            file_put_contents($save, $data);
        }
    }

}
