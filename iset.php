<?php

require_once("loader.php");

if (SHOW_ERRORS) {
    ini_set('display_errors', 1);
}

if (isset($_POST['submit'])) {
    if ($_POST['type'] == 'auth') {
        $count = Instance::getInstanceCount('auth');
        if (!$count) {
            $instance = Instance::getNew($_POST['type']);
        }
    } else {
        $instance = Instance::getNew($_POST['type']);
    }
}

if (isset($_GET['switch'])) {
    $instance = Instance::get($_GET['type'], $_GET['num']);
    if ($_GET['switch'] == 'run') {
        if ($_GET['type'] == 'auth') {
            $activeReg = Instance::getActiveInstancesCount("reg");
            if (!$activeReg) {
                $instance->run();
            } else {
                header("Location: ".WEB_PATH."iset.php?stoprun");
            }
        } else {
            $instance->run();
            header("Location: ".WEB_PATH."iset.php");
        }
    } else {
        $instance->stop();
        header("Location: ".WEB_PATH."iset.php");
    }
}

if (isset($_GET['kill'])) {
    $instance = Instance::get($_GET['type'], $_GET['num']);
    $instance->kill();
    header("Location: ".WEB_PATH."iset.php");
}

$instances = Instance::getList();
$balance = Api::checkBalance();

?>

<html>
<head>
    <title>TGBotUSERS</title>
    <style>
        body {
            font-family: Arial;
            font-size: 14px;
            padding: 15px;
        }

        table thead td {
            font-weight: bold;
        }

        table td {
            border-collapse: collapse;
            padding: 5px;
            border: 1px solid #ccc;
        }
        .active td {
            background: lightgreen;
        }
        .alert {
            padding:10px;
            color:red;
            border:1px solid #ccc;
        }
    </style>
</head>
<body>


<h2>Потоки</h2>

<? if (isset($_GET['stoprun'])): ?>
    <p class="alert">
        Для прохода по базе, выключите регистрацию новых симок.
    </p>
<? endif; ?>

<form action="" method="post">
    <h4>Добавить поток</h4>
    <select name="type">
        <option value="reg">Новые</option>
        <option value="auth">Авторизованные</option>
    </select>
    <input type="submit" name="submit" value="Добавить"/>
</form>

<? if (is_array($instances)): ?>
    <table cellspacing="0">
        <? foreach ($instances as $instance): ?>
            <tr class="<?= $instance->status == 'run' ? 'active' : '' ?>">
                <td><?= $instance->type . "_" . $instance->num; ?></td>
                <td><?= date("d/m/Y H:i:s", $instance->update_time); ?></td>
                <td>
                    <a href="<?= WEB_PATH ?>iset.php?type=<?= $instance->type ?>&num=<?= $instance->num ?>&switch=<?= $instance->status !== 'run' ? 'run' : 'stop' ?>"><?= $instance->status == 'run' ? 'Остановить' : 'Включить' ?></a>
                </td>
                <td><a href="<?= WEB_PATH ?>iset.php?type=<?= $instance->type ?>&num=<?= $instance->num ?>&kill">Удалить</a></td>
            </tr>
        <? endforeach; ?>
    </table>
<? endif; ?>

<p>
    Баланс: <?= $balance ?>
</p>

</body>

</html>