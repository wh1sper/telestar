<?php

require_once("../../loader.php");

//  основной цикл накрутки, идет пока:
//	есть невыполненное количество подписчиков во всех работах

// берет только авторизованные номера, если авторизация слетает, удаляем номер из базы

$instance_num = 1;
if (isset($argv[1])) {
    $instance_num = $argv[1];
}

define('CALLER', 'reg_' . $instance_num);

require_once 'madeline.php';

$md_settings = [
    'app_info' => ['api_id' => TG_API_ID, 'api_hash' => TG_API_HASH],
    'updates' => ['handle_old_updates' => false, 'handle_updates' => false],
];


Instance::setPid("reg", $instance_num, getmypid());

while (true) {

    if (($jobs = Job::getList(true, true)) && Api::checkBalance()) {

        Instance::updateStatus("reg", $instance_num, "run");

        Logger::log("api", "Запрашиваю номер");

        $tzid = false;

        while (!$tzid) {
            try {
                $tzid = Api::getNum();
                Instance::updateStatus("reg", $instance_num, "run");
                sleep(2);
            } catch (Exception $e) {
                sleep(2);
                Logger::log("api", "Ошибка: " . $e->getMessage());
                continue;
            }
        }

        $num = false;

        // пока не выделен номер, вешаем цикл
        while (!$num) {
            try {
                $num = Api::checkNumber($tzid);
                Instance::updateStatus("reg", $instance_num, "run");
                sleep(1);
            } catch (Exception $e) {
                Logger::log("api", "Ошибка: " . $e->getMessage());
                continue;
            }
        }

        // получили номер, а не таймаут
        if ($num) {

            if ($num === true) {
                Logger::log("api", "Проблема с номером, удаляю запрос");
            } else {
                Logger::log("api", "Выдан номер", $num);
                try {

                    if (defined('PROXY_URL')) {
                        $proxy_data = Proxy::getChecked();
                        $md_settings['connection_settings']['all']['proxy_extra'] = [
                            'address' => $proxy_data->address,
                            'port' => $proxy_data->port
                        ];
                        $md_settings['connection_settings']['all']['proxy'] = PROXY_TYPE == 'SOCKS' ? '\SocksProxy' : '\HttpProxy';
                        Logger::log("telegram", "Ставлю прокси " . $proxy_data->address);
                    }

                    $md_api = new \danog\MadelineProto\API("../../sessions/session.$num", $md_settings);

                    $md = new Md($num, $md_api);

                } catch (Exception $e) {
                    // номер сдох
                    //Sim::deleteNumber($num);
                    Logger::log("sim", "Ошибка: " . $e->getMessage(), $num);
                }

                try {
                    if ($md->auth($instance_num)) {

                        Logger::log("api", "Закрыл операцию");
                        Api::finishOperation($tzid);

                        Logger::log("telegram", "Ждем " . SLEEP_BEFORE_JOIN . " сек", $num);
                        sleep(SLEEP_BEFORE_JOIN);
                        // выполняем все активные задания
                        foreach ($jobs as $job) {
                            if ($check_status = Job::checkJob($job->channel, $num)) {
                                $md->join($job->channel);
                                Logger::log("telegram", "Номер подписался на @{$job->channel}", $num);
                                // увеличили счетчик, связали номер и канал
                                Job::increaseCount($job->id);
                                Job::numberToJob($job->channel, $num);
                                sleep(2);
                            } else {
                                Logger::log("telegram", "Номер уже подписан", $num);
                            }
                        }
                    }

                } catch (Exception $e) {
                    // номер сдох
                  //  Sim::deleteNumber($num);
                    Logger::log("sim", "Ошибка: " . $e->getMessage(), $num);
                }
            }

        }
    }

    sleep(2);
}

