<?php

// прогоняет авторизованные номера

require_once("../../loader.php");

$instance_num = 1;
if (isset($argv[1])) {
    $instance_num = $argv[1];
}

define('CALLER', 'auth_' . $instance_num);

require_once 'madeline.php';

$md_settings = [
    'app_info' => ['api_id' => TG_API_ID, 'api_hash' => TG_API_HASH],
    'updates' => ['handle_old_updates' => false, 'handle_updates' => false],
];

Instance::setPid("auth", $instance_num, getmypid());

while (true) {

    if (($jobs = Job::getList(true, true))) {

        foreach ($jobs as $job) {
            if ($num = Sim::getAuthNum($job->id)) {
                try {
                    Logger::log("sim", "Беру старый номер", $num->number);
                    if ($check_status = Job::checkJob($job->channel, $num->number)) {

                        if (defined('PROXY_URL')) {
                            $proxy_data = Proxy::getChecked();
                            $md_settings['connection_settings']['all']['proxy_extra'] = [
                                'address' => $proxy_data->address,
                                'port' => $proxy_data->port
                            ];
                            $md_settings['connection_settings']['all']['proxy'] = PROXY_TYPE == 'SOCKS' ? '\SocksProxy' : '\HttpProxy';
                            Logger::log("telegram", "Ставлю прокси " . $proxy_data->address);
                        }

                        $md_api = new \danog\MadelineProto\API("../../sessions/session.".$num->number, $md_settings);

                        $md = new Md($num->number, $md_api);

                        if ($md->join($job->channel)) {
                            Logger::log("telegram", "Номер подписался на @{$job->channel}", $num->number);
                            Job::increaseCount($job->id);
                            Job::numberToJob($job->channel, $num->number);
                        }

                    } else {
                        Logger::log("telegram", "Номер уже подписан", $num->number);
                    }
                } catch (Exception $e) {
                    // номер сдох
                    Sim::deleteNumber($num->number);
                    Logger::log("sim", "Номер забанен или не активен", $num->number);
                }
            }
        }

        sleep(2);
    }

    Instance::updateStatus("auth", $instance_num, "run");

    sleep(2);

}
