<?php


class Sim
{

    // получить запросы на номер, которые еще не получили номер
    public static function getPendingOperations()
    {
        return db()->getRows("SELECT id FROM operation");
    }

    public static function deleteOperation($tzid)
    {
        return db()->query("DELETE FROM operation WHERE id='$tzid'");
    }

    // номер выделен и готов получить смс
    public static function setNumberReady($number, $operation_id)
    {
        $date = time();
        $q1 = db()->query("INSERT INTO sim (number,date_change,operation_id) VALUES ('$number','$date','$operation_id') ON DUPLICATE KEY UPDATE number='$number',date_change='$date',operation_id='$operation_id'");
        // удаляем операцию как ненужную, дальше работаем только с получившими номер
        $q2 = self::deleteOperation($operation_id);
        return $q1 && $q2;
    }

    // взять номер со всей инфой по статусу
    // если есть jobs, не брать подписанные

    public static function getAuthNum($job = false)
    {
        $job_str = !$job ? '' : "AND number NOT IN (SELECT number FROM sim_job WHERE job_id='$job')";
        return db()->getRow("SELECT * FROM sim WHERE status='auth' $job_str ORDER BY date_change ASC LIMIT 1");
    }

    public static function getAuthNumbersCount()
    {
        return db()->getVar("SELECT COUNT(number) FROM sim WHERE status='auth'");
    }

    public static function getTzidByNumber($number)
    {
        return db()->getVar("SELECT operation_id FROM sim WHERE number='$number'");
    }

    public static function deleteNumber($num)
    {
        return db()->query("DELETE FROM sim WHERE number='$num'");
    }

    // авторизованный номер
    public static function setNumberAuth($num)
    {
        $date = time();
        return db()->query("UPDATE sim SET date_change='$date',status='auth' WHERE number='$num'");

    }

}

