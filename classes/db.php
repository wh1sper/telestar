<?php

// db singleton

class db
{

    private static $instance;
    private $mysqli;

    private function __construct()
    {
        $this->mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $this->mysqli->set_charset("utf8");
    }

    /**
     * @return db
     */
    public static function init()
    {
        if (!is_object(self::$instance)) {
            self::$instance = new db();
        }

        return self::$instance;
    }

    /**
     * @param $sql
     * @return bool|mysqli_result
     */
    public function query($sql)
    {
        return $this->mysqli->query($sql);
    }

    /**
     * @param $query
     * @return bool
     */
    public function getRows($query)
    {
        $res = db()->query($query);
        while ($row = $res->fetch_object()) {
            $rows[] = $row;
        }

        return $rows ?? false;
    }

    public function getRow($query)
    {
        $res = db()->query($query);
        return $res ? $res->fetch_object() : false;
    }

    public function getVar($query)
    {
        $res = db()->query($query);
        $data = $res->fetch_array();
        return $data ? reset($data) : false;
    }


}

function db()
{
    return db::init();
}