<?php

// madeline wrapper
// для одновременной работы нескольких скриптов, маделин нужно складывать отдельно для каждого в свою папку

class Md
{

    private $madeline;

    private $number;

    // a bit of DI
    public function __construct($number, \danog\MadelineProto\API $madeline)
    {
        $this->number = $number;
        $this->madeline = $madeline;
    }

    public function setSession($session)
    {
        $this->madeline->session = $session;
    }

    public function join($channel)
    {
        return $this->madeline->channels->joinChannel(['channel' => "@$channel"]);
    }

    // если есть сессия, пробуем взять. если отваливается, удаляем номер
    function auth($instance_num)
    {
        Logger::log("telegram", "Авторизую номер", $this->number);

        try {
            $this->madeline->phone_login($this->number);
        } catch (Exception $e) {
            return true;
        }
        
        Logger::log("telegram", "Жду смс", $this->number);

        // блокируем выполнение и циклим получение смс
        do {
            try {
                $code = Api::checkSms($this->number);
                // поток рапортует что жив
                Instance::updateStatus("reg", $instance_num, "run");
                sleep(1);
            } catch (Exception $e) {
                Logger::log("sim", "Ошибка: " . $e->getMessage(), $this->number);
            }
        } while (!$code);

        // номер не пришел
        if ($code == 'fail') {
            Logger::log("telegram", "Код не пришел, удаляю номер", $this->number);
            Sim::deleteNumber($this->number);
            return false;
        }

        try {
            $authorization = $this->madeline->complete_phone_login($code);
        } catch (Exception $e) {
            Logger::log("sim", "Ошибка: " . $e->getMessage(), $this->number);
        }

        Logger::log("telegram", "Ввожу код", $this->number);

        if ($authorization['_'] === 'account.password') {
            Logger::log("telegram", "Акаунт с паролем", $this->number);
            return false;
        }

        if ($authorization['_'] === 'account.needSignup') {

            $user = new User();
            $user->setNames();

            Logger::log("telegram", "Региcтрирую акаунт " . $user->getInfo(), $this->number);
            $signup = $this->madeline->complete_signup($user->name, $user->surname);

            if ($signup) {
                $username_rand = rand(2, 4);
                if ($username_rand != 4) {
                    Logger::log("telegram", "Ставлю username " . $user->getInfo(), $this->number);
                    $this->madeline->account->updateUsername(['username' => $user->username]);
                }

                Logger::log("telegram", "Обновляю профиль ", $this->number);
                if ($this->madeline->account->updateProfile(['first_name' => $user->name, 'last_name' => $user->surname])) {

                    // загружать ли аву? 2к1

                    $ava_rand = rand(2, 4);
                    if ($ava_rand != 4) {
                        Logger::log("telegram", "Ставлю аватар " . $user->getInfo(), $this->number);
                        $this->madeline->photos->uploadProfilePhoto(['file' => '../../' . $user->getAvatar()]);
                    }

                    Sim::setNumberAuth($this->number);
                    return true;
                }

            }

        }

        if ($authorization['_'] === 'account.Authorization') {
            Sim::setNumberAuth($this->number);
            Logger::log("sim", "Акаунт готов к работе", $this->number);
            return true;
        }


    }


}