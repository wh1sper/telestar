<?php

// потоку нужна своя копия маделин
// крон будет удалять давно не обновляющиеся потоки

class Instance
{

    private $type;
    private $num;
    private $script;
    private $dir;

    private function __construct($type, $num = false)
    {
        $this->type = $type;
        $this->script = "d_".$type.".php";
        $this->num = $num ? $num : self::getInstanceCount($type)+1;
        $this->dir = "instances/".$this->type."_".$this->num;
    }

    public static function getNew($type)
    {
        $instance = new Instance($type);
        $instance->create();
        return $instance;
    }

    public static function get($type, $num) {
        return new Instance($type, $num);
    }

    public function run()
    {
        $str = "cd {$this->dir}; nohup ".LOCAL_PHP_PATH." ".$this->script. " ".$this->num. " >> log.txt &";
        shell_exec($str);
        self::updateStatus($this->type, $this->num, "run");
        Logger::log("system","Включаю поток {$this->type}_{$this->num}");
    }

    public function stop()
    {
        $pid = db()->getVar("SELECT pid FROM instance WHERE type='{$this->type}' AND num='{$this->num}'");
        if ($pid) {
            exec("kill -9 $pid");
        }

        self::updateStatus($this->type, $this->num, "stop");
        Logger::log("system","Останавливаю поток {$this->type}_{$this->num}");
    }

    public function kill()
    {
        $this->stop();

        $files = scandir($this->dir);
        foreach ($files as $file) {
            if (strlen($file) > 1) {
                unlink($this->dir."/".$file);
            }
        }
        rmdir($this->dir);
        db()->query("DELETE FROM instance WHERE type='{$this->type}' AND num='{$this->num}'");
        Logger::log("system","Удалил поток {$this->type}_{$this->num}");
    }

    private function create()
    {
        mkdir($this->dir);
        copy("lib/madeline/madeline.php", $this->dir.'/'.'madeline.php');
        copy("lib/madeline/madeline.phar", $this->dir.'/'.'madeline.phar');
        copy("lib/madeline/madeline.phar.version", $this->dir.'/'.'madeline.phar.version');
        copy("daemons/".$this->script,$this->dir."/".$this->script);
        file_put_contents($this->dir."/log.txt","init");
        chmod($this->dir."/log.txt",0777);
        self::save($this->type,$this->num,"new");
        Logger::log("system","Создал поток {$this->type}_{$this->num}");

    }

    public static function getActiveInstancesCount($type)
    {
        return db()->getVar("SELECT COUNT(num) as num FROM instance WHERE status='run' AND type='{$type}'");
    }

    public static function getInstanceCount($type)
    {
        return db()->getVar("SELECT COUNT(num) as num FROM instance WHERE type='{$type}'");
    }

    public function getName()
    {
        return $this->type."_".$this->num;
    }

    public static function save($type, $num, $status)
    {
        $time = time();
        $q = "INSERT INTO instance (type,num,status,update_time) VALUES ('$type','$num','$status','$time') ON DUPLICATE KEY UPDATE status='$status',update_time='$time'";
        return db()->query($q);
    }

    public static function getList()
    {
        return db()->getRows("SELECT * FROM instance");
    }

    public static function updateStatus($type, $num, $status)
    {
        $time = time();
        return db()->query("UPDATE instance SET status='$status',update_time='$time' WHERE type='$type' AND num='$num'");
    }

    public static function setPid($type, $num, $pid)
    {
        $time = time();
        return db()->query("UPDATE instance SET pid='$pid',update_time='$time' WHERE type='$type' AND num='$num'");
    }

    // убиваем все умершие потоки ( не обновлялись более 20 секунд )
    public static function clear()
    {
        $time = time() - 20;
        $instances = db()->getRows("SELECT type,num FROM instances WHERE update_time < $time");
        if (!$instances) return false;

        $count = 0;

        foreach ($instances as $inst) {
            $instance = self::get($inst->type,$inst->num);
            if ($instance->kill()) {
                $count++;
            }
        }

        Logger::log("system","Удалил потоков: $count");

        return $count;
    }


}