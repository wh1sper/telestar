<?php


class API
{

    /**
     * @param $url
     * @param string $method
     * @return json
     */
    private static function call($url, $method = 'GET')
    {
        echo date("API d/M/Y H:i:s")." API CALL ".$url."\r\n";
        $result = file_get_contents($url, false, stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))));
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        $result = trim(curl_exec($ch));
//        curl_close($ch);

        if (!$result) {
            Logger::log("api", "Апи не доступен");
            return false;
        }

        $res = json_decode($result);

        if (isset($res->response)) {
            echo date("API d/M/Y H:i:s")." ".$res->response."\r\n";
        } else {
            if (is_array($res)) {
                $res = reset($res);
                if (isset($res->response)) {
                    echo date("API d/M/Y H:i:s")." ".$res->response."\r\n";
                }
            } else {
                var_dump($res);
            }
        }

        return $res;
    }

    public static function checkBalance()
    {
        $url  = "https://onlinesim.ru/api/getBalance.php?apikey=". SIM_API_KEY;
        $result = self::call($url);

        if (!$result) return 0;

        return $result->balance > 3.00 ? $result->balance : 0;

    }

    // запрос на выдачу номера. создается операция с tzid
    public static function getNum()
    {

        $url = "https://onlinesim.ru/api/getNum.php?apikey=" . SIM_API_KEY . "&service=telegram&country=". SIM_API_COUNTRY;
        $result = self::call($url);

        if (!$result) return false;

        $thread = CALLER;

        if ($result->response && isset($result->tzid)) {
            if (db()->query("INSERT INTO operation SET id='{$result->tzid}',thread='$thread'")) {
                $ret = $result->tzid;
            }
        }

        return $ret ?? false;
    }


    public static function finishOperation($tzid)
    {
        $url = "https://onlinesim.ru/api/setOperationOk.php?apikey=" . SIM_API_KEY . "&tzid=" . $tzid;
        $result = self::call($url);

        if (!is_array($result)) {
            return false;
        }

        if ($result->response === 1) {
            return true;
        } else {
            return false;
        }
    }


    // проверить, выдали ли номер на запрос tzid
    public static function checkNumber($tzid)
    {
        $url = "https://onlinesim.ru/api/getState.php?apikey=" . SIM_API_KEY . "&tzid=" . $tzid;
        $result = self::call($url);

        if (!is_array($result)) {
            return false;
        }

        $result = reset($result);

        // нет номеров
        if (!$result->response == 'WARNING_NO_NUMS') {
            Sim::deleteOperation($tzid);
            return true;
        }

        // номер выдан, готов получить смс
        if (isset($result->number)) {
            Sim::setNumberReady($result->number, $tzid);
            return $result->number;
        }

        return false;
    }

    public static function checkSms($number)
    {

        $tzid = Sim::getTzidByNumber($number);

        $url = "https://onlinesim.ru/api/getState.php?apikey=" . SIM_API_KEY . "&message_to_code=1&tzid=" . $tzid;
        $result = self::call($url);

        if (!is_array($result)) {
            return false;
        }

        $result = reset($result);

        if ($result->response == 'TZ_NUM_ANSWER') {
            if (isset($result->msg)) {
                return $result->msg;
            }
        }

        $time_wait = (60*20) - SMS_WAIT_SEC;
        if ($result->response == 'TZ_OVER_EMPTY' || (isset($result->time) && $result->time < $time_wait)) {
            return 'fail';
        }

        return false;

    }

}