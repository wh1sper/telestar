<?php


class Logger
{

    private static $instance;

    private function  __construct()
    {
        $this->mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    }

    /**
     * @return logger
     */
    public static function init()
    {
        if(!is_object(self::$instance)) {
            self::$instance = new logger();
        }

        return self::$instance;
    }

    public static function log($type, $text, $num = false)
    {
        $date = time();

        $num_str = $num ? ",num='$num'" : '';

        $thread = defined('CALLER') ? CALLER : 'system';

        db()->query("INSERT INTO log SET thread='$thread',type='$type',text='$text',date='$date' $num_str");
    }

    public static function get($limit)
    {
        return db()->getRows("SELECT * FROM log ORDER BY date DESC LIMIT $limit");
    }

}

function logger()
{
    return logger::init();
}