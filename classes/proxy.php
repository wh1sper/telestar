<?php


class Proxy
{

    public static function getChecked()
    {
        do {
            $proxy = self::get();
            Logger::log("system", "Проверяю прокси $proxy");
            $status = self::check($proxy);
            Logger::log("system", "Прокси $proxy - ".$status ? "ok" : "bad");
        } while (!$status);

        return $proxy;
    }

    public static function check($proxy)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://telegram.org");
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
        if (PROXY_TYPE == 'SOCKS') {
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
        }
        $res = curl_exec($ch);
        curl_close($ch);
        return strlen($res) > 100 ? true : false;
    }

    public static function get()
    {
        $proxy = db()->getRow("SELECT proxy FROM proxy WHERE is_used=0 ORDER BY id ASC LIMIT 1");
        if ($proxy) {
            db()->query("UPDATE proxy SET is_used=1 WHERE id='".$proxy->id."'");
        } else {
            db()->query("UPDATE proxy SET is_used=0");
            $proxy = db()->getRow("SELECT * FROM proxy WHERE is_used=0 ORDER BY id ASC LIMIT 1");
        }

        $proxy_arr = explode(":",$proxy);

        $proxy_obj = (object)['address' => $proxy_arr[0], 'port' => $proxy_arr[1]];

        return $proxy_obj;
    }

    public static function parse()
    {
        $proxies = file(PROXY_URL);
        foreach ($proxies as &$proxy) {
            $proxy = trim($proxy);
        }

        if (sizeof($proxies)) {
            $db_proxies = db()->getRows("SELECT id,proxy FROM proxy");

            // удалим из бд те, которых нет в файле
            foreach ($db_proxies as $prox) {
                if (!in_array($prox->proxy, $proxies)) {
                    db()->query("DELETE FROM proxy WHERE id='" . $prox->id . "'");
                }
            }

            // добавим в бд новые
            foreach ($proxies as $prox) {
                db()->query("INSERT IGNORE INTO proxy SET proxy='$prox'");
            }

        }

    }

}