<?php

class Job
{

    public static function create($channel,$amount)
    {
        return db()->query("INSERT INTO job SET channel='$channel',amount='$amount'");
    }

    public static function delete($job_id)
    {
        return db()->query("DELETE FROM job WHERE id='$job_id'");
    }

    public static function increaseCount($job_id)
    {
        return db()->query("UPDATE job SET done=done+1 WHERE id='$job_id'");
    }

    public static function changeStatus($job_id,$status)
    {
        return db()->query("UPDATE job SET active='$status' WHERE id='$job_id'");
    }

    public static function getList($active = false, $undone = false)
    {
        if ($active === false) {
            $active_str = "(active=0 OR active=1)";
        } else {
            $active_str = $active ? "active=1" : "active=0";
        }

        $undone_str = $undone ? "AND done < amount" : '';

        $str = "SELECT * FROM job WHERE $active_str $undone_str ORDER BY id DESC";
        return db()->getRows($str);
    }

    // проверка, не подписан ли данный номер уже на канал
    public static function checkJob($channel, $num)
    {
        $q = "SELECT id FROM sim_job WHERE channel='$channel' AND number='$num'";
        $done_query = db()->getRow($q);
        return !$done_query;
    }

    public static function numberToJob($channel, $num)
    {
        $date = time();
        return db()->query("INSERT INTO sim_job SET channel='$channel',number='$num',date='$date'");
    }

    public static function getTotalSubscribers($channel)
    {
        return db()->getVar("SELECT COUNT(id) FROM sim_job WHERE channel='$channel'");
    }

    public static function stopAll()
    {
        db()->query("UPDATE job SET active=0");
    }


}