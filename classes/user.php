<?php

class User
{

    public $name;
    public $surname = '';

    public $username = '';
    public $avatar;

    private $name_db;
    private $surname_db;

    private $sex;

    public function __construct()
    {
        $this->sex = rand(0, 1);
        $this->name_db = db()->getRow("SELECT name,name_ru FROM name WHERE sex='{$this->sex}' ORDER BY rand() LIMIT 1");
        $this->surname_db = db()->getRow("SELECT name,name_ru FROM surname WHERE sex='{$this->sex}' ORDER BY rand() LIMIT 1");
        $this->setUsername();
    }

    public function setNames()
    {

        // 1 - ru name
        // 2 - en name
        // 3 - ru name + ru surname
        // 4 - en name + en surname
        // 5 - username

        $type = rand(1, 4);
        if ($type === 1) {
            $this->name = $this->name_db->name_ru;
        }

        if ($type == 2) {
            $this->name = $this->name_db->name;
        }

        if ($type == 3) {
            $this->name = $this->name_db->name_ru;
            $this->surname = $this->surname_db->name_ru;
        }

        if ($type == 4) {
            $this->name = $this->name_db->name;
            $this->surname = $this->surname_db->name;
        }
//
//        if ($type == 5) {
//            $this->name = $this->username;
//        }

    }

    private function setUsername()
    {
        // 1 - numbername
        // 2 - surnamenumber
        // 3 - namesurnamenumber
        // 4 - name_surname_number

        $type = rand(1, 8);

        if ($type === 1) {
            $this->username = strtolower($this->name_db->name) . rand(1000, 100000);
        }

        if ($type == 2) {
            $this->username = strtolower($this->surname_db->name) . rand(1000, 100000);
        }

        if ($type == 3) {
            $this->username = strtolower($this->name_db->name) . strtolower($this->surname_db->name) . rand(1000, 100000);
        }

        if ($type == 4) {
            $this->username = strtolower($this->name_db->name) . '_' . rand(1000, 100000);
        }

        if ($type == 5) {
            $this->username = strtolower($this->name_db->name) . strtolower($this->surname_db->name)[0] . rand(1, 100);
        }

        if ($type == 6) {
            $this->username = strtolower($this->surname_db->name)[0] . strtolower($this->name_db->name). rand(1, 100);
        }

        if ($type == 7) {
            $this->username = strtolower($this->name_db->name) . "_".strtolower($this->surname_db->name)[0] . rand(1, 100);
        }

        if ($type == 8) {
            $this->username = strtolower($this->surname_db->name)[0] ."_". strtolower($this->name_db->name) . rand(1, 100);
        }


    }

    public function getAvatar()
    {
        $av = db()->getRow("SELECT * FROM avatar WHERE sex='{$this->sex}' AND is_used=0 ORDER BY RAND()");
        if (!$av) {
            db()->query("UPDATE avatar SET is_used=0 WHERE sex='{$this->sex}'");
            $av = db()->getRow("SELECT * FROM avatar WHERE sex='{$this->sex}' AND is_used=0 ORDER BY RAND()");
        }

        $dir = $this->sex === 1 ? "male" : "female";
        $image = "images/$dir/" . $av->image;

        db()->query("UPDATE avatar SET is_used=1 WHERE id='{$av->id}'");

        return $image;

    }

    public function getInfo()
    {
        return (strlen($this->username) > 1 ? "@".$this->username : '')." ".$this->name." ".$this->surname;
    }


}