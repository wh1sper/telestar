<html>
<head>
    <title>TGBotUSERS</title>
    <style>
        body {
            font-family: Arial;
            font-size: 14px;
            padding: 15px;
        }

        table thead td {
            font-weight: bold;
        }

        table td {
            border-collapse: collapse;
            padding: 5px;
            border: 1px solid #ccc;
        }

        div.add {
            padding-top: 15px;
        }

        .button {
            background: lightblue;
            padding: 5px;
            border: 1px solid #ccc;
            display: block;
            margin-top: 10px;
            cursor: pointer;
        }

        .job_0 td {
            background: #f3d5da;
        }

        a {
            font-size: 11px;
        }

        .block1 {
            float: left;
            padding-right:50px;
            width: 40%;
        }

        .block2 {
            float: right;
            width: 40%;
        }

        .log_thread {
            color: #ccc;
        }

        .log_type {
            font-weight: bold;
        }

        .log_date {
            font-style: italic;
        }

        .clear {
            clear: both;
        }

    </style>
</head>

<body>

<div class="block1">

    <h1>Задания</h1>

    <? if (isset($job_error)): ?>
        <p>Название канала не должно содержать спецсимволы</p>
    <? endif; ?>

    <table cellspacing="0">
        <thead>
        <tr>
            <td>#</td>
            <td>Канал</td>
            <td>Количество</td>
            <td>Сумма канала</td>
            <td>Статус</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        </thead>
        <tbody>
        <? if ($jobs): ?>
            <? foreach ($jobs as $k => $job): ?>
                <tr class="job_<?= $job->active ?>">
                    <td><?= ++$k ?></td>
                    <td><?= $job->channel ?></td>
                    <td><?= $job->done ?> / <?= $job->amount ?></td>
                    <td><?= Job::getTotalSubscribers($job->channel); ?></td>
                    <td><?= $job->active ? "Активно" : "Не активно" ?></td>
                    <td>
                        <a href="<?= WEB_PATH ?>?changeJobStatus&job_id=<?= $job->id ?>&status=<?= $job->active ? 0 : 1 ?>">
                            <?= $job->active ? "Выключить" : "Включить" ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?= WEB_PATH ?>?deleteJob&job_id=<?= $job->id ?>">Удалить</a>
                    </td>
                </tr>
            <? endforeach; ?>
        <? endif; ?>
        </tbody>
    </table>

    <? if (!$jobs): ?>
        <div class="add">
            <h2>Добавить канал в работу</h2>
            <form method="post" action="">
                <input type="text" name="channel" placeholder="Канал"/>
                <input type="text" name="amount" placeholder="Количество"/>
                <input class="button" type="submit" name="submit" value="Добавить"/>
            </form>

        </div>
    <? endif; ?>

    <p>
        Авторизованных симок: <?= $auth_count ?? 0; ?>
    </p>

</div>

<div class="block2">
    <h1>Лог</h1>
    <div class="log">
        <? if ($log): ?>
            <? foreach ($log as $record): ?>
                <div>
                    <span class="log_date"><?= date("d/m/Y H:i:s", $record->date); ?></span>
                    <span class="log_type"><?= $record->type ?></span>
                    <span class="log_number"><?= $record->num ?></span>
                    <span class="log_action"><?= $record->text ?></span>
                    <span class="log_thread"><?= $record->thread ?></span>
                </div>
            <? endforeach; ?>
        <? endif; ?>
    </div>
</div>


</body>

</html>