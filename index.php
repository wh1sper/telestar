<?php

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
} else {
    if($_SERVER['PHP_AUTH_USER'] !== 'admin' && $_SERVER['PHP_AUTH_PW'] !== 'mostg') {
        die();
    }
}

require_once("loader.php");

if (SHOW_ERRORS) {
    ini_set('display_errors', 1);
}

if (isset($_GET['changeJobStatus'])) {
    Job::changeStatus($_GET['job_id'],$_GET['status']);
    header("Location: ".WEB_PATH."index.php");
}

if (isset($_GET['deleteJob'])) {
    Job::delete($_GET['job_id']);
    header("Location: ".WEB_PATH."index.php");
}

if (isset($_POST['submit'])) {
    if (strpos($_POST['channel'],'/') === false) {
        Job::create($_POST['channel'],$_POST['amount']);
    } else {
        $job_error = true;
    }
}

$jobs = Job::getList();
$auth_count = Sim::getAuthNumbersCount();
$log = Logger::get(100);

require_once("template.php");




